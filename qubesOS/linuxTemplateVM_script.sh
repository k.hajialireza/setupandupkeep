#!/usr/bin/env bash

. update_functions.sh

# - firmware?
# - os-upgrade?
# stage1 tools   stage2 tools  based on dependency?  (or repos)    example: dnf-plugins-core, curl, wget, jq?
# zulucrypt-console\

# Firefox
#  wie mit cli?
#	- Bitwarden Addon 

# Vault? Keypass?
#
# cargo npm and other language specific package managers?


for platform in 'fedora' 'debian' 'whonix-workstation' 'flatpak'; do
	eval "${platform}_tools_cli=\$(rg '\"interface\":\"cli\"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '${platform}\":null' | sed 's/.*${platform}\":\\([^,]*\\).*/\\1/' | uniq | head -n 1)"
	eval "${platform}_tools_gui=\$(rg '\"interface\":\"gui\"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '${platform}\":null' | sed 's/.*${platform}\":\\([^,]*\\).*/\\1/' | uniq | head -n 1)"
done
#fedora_tools_cli=($(rg '"interface":"cli"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '".rpm_fedora":null' | sed 's/.*".rpm_fedora":\([^,]*\).*/\1/' | uniq))
#fedora_tools_gui=($(rg '"interface":"gui"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '".rpm_fedora":null' | sed 's/.*".rpm_fedora":\([^,]*\).*/\1/' | uniq))
#flatpak_tools_cli=($(rg '"interface":"cli"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '"flatpak":null' | sed 's/.*"flatpak":\([^,]*\).*/\1/' | uniq))
#flatpak_tools_gui=($(rg '"interface":"gui"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '"flatpak":null' | sed 's/.*"flatpak":\([^,]*\).*/\1/' | uniq))

# ADDITIONAL REPOSITORIES
# - brave-browser (last check: ?)
#   https://brave.com/linux/
# - powershell
#   https://www.tecmint.com/install-powershell-on-fedora-linux/
#   https://learn.microsoft.com/en-us/linux/packages
#   https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-linux?view=powershell-7.3
# - containerlab (last check: 2024-07-27)
#   https://containerlab.dev/install

package_manager_update
if grep -q '^ID=fedora' /etc/os-release; then
	sudo dnf install dnf-plugins-core wget curl git
	sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo
	sudo rpm --import ~/downloads_dispvm/brave-core.asc
	sudo dnf install brave-browser brave-keyring

	sudo rpm --import ~/downloads_dispvm/microsoft.asc
	sudo rpm -i ~/downloads_dispvm/packages-microsoft-prod.rpm
	sudo dnf config-manager --add-repo https://packages.microsoft.com/yumrepos/edge

	dnf-config-manager --add-repo=https://netdevops.fury.site/yum/ && \
	echo "gpgcheck=0" | sudo tee -a /etc/yum.repos.d/yum.fury.io_netdevops_.repo

elif grep -q '^ID=debian' /etc/os-release; then
	sudo apt install wget curl git
	mv ~/downloads_dispvm/brave-browser-archive-keyring.gpg /usr/share/keyrings/brave-browser-archive-keyring.gpg
	grep 'deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main' /etc/apt/sources.list.d/brave-browser-release.list ||
		sudo echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main" >>/etc/apt/sources.list.d/brave-browser-release.list

	#powershell

	echo "deb [trusted=yes] https://netdevops.fury.site/apt/ /" | \
	sudo tee -a /etc/apt/sources.list.d/netdevops.list

fi

package_manager_update
if grep -q '^ID=fedora' /etc/os-release; then
	sudo dnf install ${fedora_tools_cli[@]} ${fedora_tools_gui[@]}
elif grep -q '^ID=debian' /etc/os-release; then
	sudo apt install ${debian_tools_cli[@]} ${debian_tools_gui[@]}
fi

# Arduino (last check: 12.08.2023)
bash ~/downloads_dispvm/arduino-cli/install.sh

# Cisco PacketTracer
if grep -q '^ID=fedora' /etc/os-release; then
	bash ~/downloads_dispvm/packettracer-fedora/install.sh -d ~/downloads_dispvm/
elif grep -q '^ID=debian' /etc/os-release; then
	sudo apt install ~/downloads_dispvm/CiscoPacketTracer822_amd64_signed.deb
fi

# Syncthing Service
#	- In QubeManager den Service Anlegen (bzw. Kommando finden)
[[ -d /etc/systemd/system/syncthing@user.service.d ]] ||\
 sudo mkdir /etc/systemd/system/syncthing@user.service.d
[[ ! -f /etc/systemd/system/syncthing@user.service.d/30_qubes.conf ]] &&\
 sudo echo "[Unit]\nConditionPathExists=/var/run/qubes-service/syncthing@user" >\
 /etc/systemd/system/syncthing@user.service.d/30_qubes.conf
sudo systemctl enable syncthing
#if [[ ! -f /etc/systemd/system/syncthing@user.service.d/30_qubes.conf ]]; then
#	echo "[Unit]" > /etc/systemd/system/syncthing@user.service.d/30_qubes.conf
#	echo "ConditionPathExists=/var/run/qubes-service/syncthing@user" >> /etc/systemd/system/syncthing@user.service.d/30_qubes.conf
#fi

# Wireguard
#  config + keys
if ! [[ -d $HOME/wireguard ]] ;then
	sudo mv /etc/wireguard $HOME
	sudo ln -s $HOME/wireguard /etc/wireguard
fi

# Docker
# - https://docs.docker.com/desktop/install/fedora/ 2023-09-16
# - https://docs.docker.com/desktop/install/debian/ 2023-09-16
#if grep -q '^ID=fedora' /etc/os-release; then
#	sudo dnf remove docker\
#		docker-client\
#		docker-client-latest\
#		docker-common\
#		docker-latest\
#		docker-latest-logrotate\
#		docker-logrotate\
#		docker-selinux\
#		docker-engine-selinux\
#		docker-engine
#	#sudo dnf install dnf-plugins-core
#	sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
#	sudo dnf install\
#		docker-ce\
#		docker-ce-cli\
#		containerd.io\
#		docker-buildx-plugin\
#		docker-compose-plugin
#elif grep -q '^ID=debian' /etc/os-release; then
#	rm -r $HOME/.docker/desktop
#	sudo rm /usr/local/bin/com.docker.cli
#	sudo apt purge docker-desktop
#	sudo apt install ./docker-desktop-<version>-<arch>.deb
#fi

# dotfiles
#	vimrc und andere sync sachen? symlinks?
# sudo? mit sudo ln SCHEINT user user zu sein
# $HOME/.config/nvim
# $HOME/.bashrc
# $HOME/.bashrc.d
# $HOME/.newsboat
# $HOME/.task
# $HOME/.taskrc
# $HOME/Documents
# $HOME/vimwiki
# $HOME/ wireguard?
# dotfiles_public
# SetupAndUpkeep


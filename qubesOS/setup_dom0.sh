#!/usr/bin/env bash

# TODO
# - polybar necassary?
#	- how even all the i3 status stuff?
# - autotiling
#	- how to write this to not do it when it already exists?
#	- how to do it with iostream and curl?  maybe with interactive code review beforehand?

[ -d ~/bin ] || mkdir ~/bin

appvm=untrusted

sudo qubes-dom0-update\
	i3\
	i3-settings-qubes\
	python3-i3ipc\
	rofi
	#qubes-windows-tools-4.1.69
	#onboard
	#ansible-python3
	#polybar

# Switch CAPS and ESC
#localectl set-x11-keymap de nodeadkeys,caps:swapescape

# download stuff in disposableVM and transfer to vault
#https://github.com/elliotkillick/qvm-create-windows-qube 
#qvm-run --pass-io appvm -- 'cat /home/user/SetupAndUpkeep/qubesOS/dispvm.sh' | qvm-run --pass-io -a --dispvm -- 'bash -s'

# WacomTablet
grep "sys-usb dom0 allow" /etc/qubes-rpc/policy/qubes.InputTablet ||
	echo -e "sys-usb dom0 allow\n$(cat /etc/qubes-rpc/policy/qubes.InputTablet)" | sudo tee /etc/qubes-rpc/policy/qubes.InputTablet

# Bluetooth Dongle Mouse&Keyboard
# - Mouse even necessary? done automatic?
# https://www.qubes-os.org/doc/usb-qubes/
grep "sys-usb dom0 ask,default_target=dom0" /etc/qubes-rpc/policy/qubes.InputKeyboard ||
	echo -e "sys-usb dom0 ask,default_target=dom0\n$(cat /etc/qubes-rpc/policy/qubes.InputKeyboard)" | sudo tee /etc/qubes-rpc/policy/qubes.InputKeyboard
#grep "sys-usb dom0 ask,user=root,default_target=dom0" /etc/qubes-rpc/policy/qubes.InputMouse ||
	echo -e "sys-usb dom0 ask,user=root,default_target=dom0\n$(cat /etc/qubes-rpc/policy/qubes.InputMouse)" | sudo tee /etc/qubes-rpc/policy/qubes.InputMouse

# intel graphics
# https://forum.qubes-os.org/t/intel-integrated-graphics-troubleshooting/19081
[[ -f /etc/X11/xorg.conf.d/20-intel.conf ]] ||
	sudo cat >/etc/X11/xorg.conf.d/20-intel.conf << EOF
Section "Device"
    Identifier "Intel Graphics"
    Driver "Intel"
    # UXA is more stable than the default SNA for some users
    #Option "AccelMethod" "UXA"
EndSection
EOF

[[ -f /etc/udev/rules.d/99-usbKeyboard.rules ]] ||
	sudo cat >/etc/udev/rules.d/99-usbKeyboard.rules << EOF
SUBSYSTEM=="input", ATTRS{name}=="*[Kk]eyboard*", ACTION=="add", ENV{XKBOPTIONS}="caps:swapescape"
EOF
[[ -f /etc/X11/xinit/xinitrc.d/autoStart.sh ]] ||
	sudo cat >/etc/X11/xinit/xinitrc.d/autoStart.sh << EOF
#!/usr/bin/env bash

setxkbmap -option caps:swapescape

xinput_touchpad=$(xinput list | grep "Elan Touchpad" | sed 's/.*id=\([0-9]\+\).*$/\1/')
touchpad_tapToClick_option=$(xinput list-props $xinput_touchpad | grep "Tapping Enabled ([0-9]\+)" | sed 's/.*(\([0-9]\+\)).*$/\1/')
xinput set-prop $xinput_touchpad $touchpad_tapToClick_option 1

xsetwacom set "sys-usb: Wacom Pen and multitouch sensor Finger touch" Gesture on

if [[ $(xrandr | grep -c " connected") == 3 ]]; then
        xrandr --output DP1 --auto --primary --right-of eDP1 --output HDMI2 --auto --right-of DP1
fi
EOF

# i3
# top bar
grep "position top" ~/.config/i3/config ||
	sed -i 's/^bar {.*/&\n\tposition top/' ~/.config/i3/config
grep "tray_output primary" ~/.config/i3/config ||
	sed -i 's/^bar {\n\tposition top/.*/&\n\ttray_output primary/' ~/.config/i3/config
# polybar
# default terminal bindsim return
grep "xfce4-terminal" ~/.config/i3/config ||
	sed 's/^\(bindsym $mod+Return exec\).*/\1 xfce4-terminal/' ~/.config/i3/config
# dmenu
#grep '--dmenu=\"dmenu -i' ~/.config/i3/config || 
#	sed -i 's/--dmenu="dmenu/& -i/' ~/.config/i3/config
# rofi
grep "rofi" ~/.config/i3/config ||
	sed -i 's/\(.*\)i3-dmenu.*/#&\n\1rofi -show drun/' ~/.config/i3/config

# - autotiling
#@AppVM: wget https://raw.githubusercontent.com/ElliotKillick/qvm-create-windows-qube/master/install.sh
[[ -f $HOME/bin/autotiling.py ]] || qvm-run -p --filter-escape-chars --no-color-output $appvm "cat '/home/user/Downloads/autotiling.py'" > $HOME/bin/autotiling.py
cat >~/bin/autotiling.py << EOF
#!/usr/bin/env python3

# copied 2023-09-02
#   wget --output-document ~/bin/autotiling.py https://raw.githubusercontent.com/nwg-piotr/autotiling/master/autotiling/main.py
"""
This script uses the i3ipc python module to switch the layout splith / splitv
for the currently focused window, depending on its dimensions.
It works on both sway and i3 window managers.

Inspired by https://github.com/olemartinorg/i3-alternating-layout

Copyright: 2019-2021 Piotr Miller & Contributors
e-mail: nwg.piotr@gmail.com
Project: https://github.com/nwg-piotr/autotiling
License: GPL3

Dependencies: python-i3ipc>=2.0.1 (i3ipc-python)
"""
import argparse
import os
import sys
from functools import partial

from i3ipc import Connection, Event

try:
    from .__about__ import __version__
except ImportError:
    __version__ = "unknown"


def temp_dir():
    return os.getenv("TMPDIR") or os.getenv("TEMP") or os.getenv("TMP") or "/tmp"


def save_string(string, file_path):
    try:
        with open(file_path, "wt") as file:
            file.write(string)
    except Exception as e:
        print(e)


def output_name(con):
    if con.type == "root":
        return None

    if p := con.parent:
        if p.type == "output":
            return p.name
        else:
            return output_name(p)


def switch_splitting(i3, e, debug, outputs, workspaces, depth_limit):
    try:
        con = i3.get_tree().find_focused()
        output = output_name(con)
        # Stop, if outputs is set and current output is not in the selection
        if outputs and output not in outputs:
            if debug:
                print(f"Debug: Autotiling turned off on output {output}", file=sys.stderr)
            return

        if con and not workspaces or (str(con.workspace().num) in workspaces):
            if con.floating:
                # We're on i3: on sway it would be None
                # May be 'auto_on' or 'user_on'
                is_floating = "_on" in con.floating
            else:
                # We are on sway
                is_floating = con.type == "floating_con"

            if depth_limit:
                # Assume we reached the depth limit, unless we can find a workspace
                depth_limit_reached = True
                current_con = con
                current_depth = 0
                while current_depth < depth_limit:
                    # Check if we found the workspace of the current container
                    if current_con.type == "workspace":
                        # Found the workspace within the depth limitation
                        depth_limit_reached = False
                        break

                    # Look at the parent for next iteration
                    current_con = current_con.parent

                    # Only count up the depth, if the container has more than
                    # one container as child
                    if len(current_con.nodes) > 1:
                        current_depth += 1

                if depth_limit_reached:
                    if debug:
                        print("Debug: Depth limit reached")
                    return

            is_full_screen = con.fullscreen_mode == 1
            is_stacked = con.parent.layout == "stacked"
            is_tabbed = con.parent.layout == "tabbed"

            # Exclude floating containers, stacked layouts, tabbed layouts and full screen mode
            if (not is_floating
                    and not is_stacked
                    and not is_tabbed
                    and not is_full_screen):
                new_layout = "splitv" if con.rect.height > con.rect.width else "splith"

                if new_layout != con.parent.layout:
                    result = i3.command(new_layout)
                    if result[0].success and debug:
                        print(f"Debug: Switched to {new_layout}", file=sys.stderr)
                    elif debug:
                        print(f"Error: Switch failed with err {result[0].error}", file=sys.stderr)

        elif debug:
            print("Debug: No focused container found or autotiling on the workspace turned off", file=sys.stderr)

    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--debug", action="store_true",
                        help="print debug messages to stderr")
    parser.add_argument("-v", "--version", action="version",
                        version=f"%(prog)s {__version__}, Python {sys.version}",
                        help="display version information")
    parser.add_argument("-o", "--outputs", nargs="*", type=str, default=[],
                        help="restricts autotiling to certain output; example: autotiling --output  DP-1 HDMI-0")
    parser.add_argument("-w", "--workspaces", nargs="*", type=str, default=[],
                        help="restricts autotiling to certain workspaces; example: autotiling --workspaces 8 9")
    parser.add_argument("-l", "--limit", type=int, default=0,
                        help='limit how often autotiling will split a container; try "2" if you like master-stack layouts; default: 0 (no limit)')
    """
    Changing event subscription has already been the objective of several pull request. To avoid doing this again
    and again, let's allow to specify them in the `--events` argument.
    """
    parser.add_argument("-e", "--events", nargs="*", type=str, default=["WINDOW", "MODE"],
                        help="list of events to trigger switching split orientation; default: WINDOW MODE")

    args = parser.parse_args()

    if args.debug:
        if args.outputs:
            print(f"autotiling is only active on outputs: {','.join(args.outputs)}")
        if args.workspaces:
            print(f"autotiling is only active on workspaces: {','.join(args.workspaces)}")

    # For use w/ nwg-panel
    ws_file = os.path.join(temp_dir(), "autotiling")
    if args.workspaces:
        save_string(','.join(args.workspaces), ws_file)
    else:
        if os.path.isfile(ws_file):
            os.remove(ws_file)

    if not args.events:
        print("No events specified", file=sys.stderr)
        sys.exit(1)

    handler = partial(
        switch_splitting,
        debug=args.debug,
        outputs=args.outputs,
        workspaces=args.workspaces,
        depth_limit=args.limit,
    )
    i3 = Connection()
    for e in args.events:
        try:
            i3.on(Event[e], handler)
            print(f"{Event[e]} subscribed")
        except KeyError:
            print(f"'{e}' is not a valid event", file=sys.stderr)

    i3.main()


if __name__ == "__main__":
    main()
EOF
grep "exec_always --no-startup-id autotiling.py" ~/.config/i3/config ||
	echo -e "\nexec_always --no-startup-id autotiling.py" >> ~/.config/i3/config
chmod +x ~/bin/autotiling.py

# enable networking

# template setup and appVM setup (xfce templates?)
## all templates necessary installed?
#qvm-ls
#qvm-template list --availbable

# Windows Qubes
# https://github.com/elliotkillick/qvm-create-windows-qube
#@AppVM: wget https://raw.githubusercontent.com/ElliotKillick/qvm-create-windows-qube/master/install.sh
 qvm-run -p --filter-escape-chars --no-color-output $appvm "cat '/home/user/Downloads/install.sh'" > $HOME/bin/qvm-create_WindowsQube_install.sh

# download image with mido in dispvm -> copy to windows-mgt
qvm-create-windows-qube -n sys-firewall -oyt -i win10x64-enterprise-ltsc-eval.iso -a win10x64-enterprise-ltsc-eval.xml windows-10-ltsc


# Ansible to control Qubes
# https://qubes-ansible.readthedocs.io/en/latest/install.html 2023-11-19
#sudo su -
#mkdir -p /usr/share/ansible_module/conns
#qvm-run --pass-io development 'cat /home/user/qubes_ansible/ansible_module/qubesos.py' > /usr/share/ansible_module/qubesos.py
#qvm-run --pass-io development 'cat /home/user/qubes_ansible/ansible_module/conns/qubes.py' > /usr/share/ansible_module/conns/qubes.py
#echo "library = /usr/share/ansible_module/" >>/etc/ansible/ansible.cfg
#echo "connection_plugins = /usr/share/ansible_module/conns/" >>/etc/ansible/ansible.cfg


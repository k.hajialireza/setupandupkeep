#!/usr/bin/env bash

appvm=untrusted
templatevm=fedora-40

cat downloads_dispVM.sh |
	qvm-run --pass-io --autostart --dispvm -- 'bash -s'

qvm-run --pass-io --autostart $templatevm -- 'ls ~/QubesIncoming/disp*'
exit

script=download_dispVm.sh

qvm-run --pass-io $appvm -- "cat $script" |
	qvm-run --pass-io $templatevm -- 'bash -s'

#qvm-run -p --filter-escape-chars --no-color-output $appvm "cat '/home/user/Downloads/install.sh'" > $HOME/bin/qvm-create_WindowsQube_install.sh

#!/usr/bin/env bash

sudo dnf install -y curl wget git

mkdir ~/downloads_dispvm
cd ~/downloads_dispvm

git clone https://gitlab.com/k.hajialireza/SetupAndUpkeep
git clone https://gitlab.com/k.hajialireza/dotfiles public

# - qubes-windows-tools
mkdir ~/downloads_dispvm/windows_tools
curl https://raw.githubusercontent.com/ElliotKillick/qvm-create-windows-qube/master/install.sh >~/downloads_dispvm/windows_tools/install.sh
#qvm-copy windows_tools

# - brave-browser
wget https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
#qvm-copy brave-core.asc
sudo curl -fsSLo brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
#qvm-copy brave-browser-archive-keyring.gpg 

# - powershell
#curl -sSL -O https://packages.microsoft.com/config/fedora/38/packages-microsoft-prod.rpm
curl -sSL -O https://packages.microsoft.com/config/fedora/40/packages-microsoft-prod.rpm
curl -sSL -O https://packages.microsoft.com/config/rhel/9/packages-microsoft-prod.rpm
wget https://packages.microsoft.com/keys/microsoft.asc
#qvm-copy microsoft.asc
#curl -sSL -O https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.rpm

# - packettracer
# Download packettracer
# https://www.netacad.com/portal/resources/packet-tracer
# https://www.computernetworkingnotes.com/ccna-study-guide/download-packet-tracer-for-windows-and-linux.html
wget https://archive.org/download/cpt822/CiscoPacketTracer822_amd64_signed.deb
git clone https://github.com/thiagoojack/packettracer-fedora

# - arduino-cli
mkdir ~/downloads_dispvm/arduino-cli
curl -fsSLo ~/downloads_dispvm/arduino-cli/install.sh https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh

qvm-copy ~/downloads_dispvm

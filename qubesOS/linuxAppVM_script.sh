#!/usr/bin/env bash

. update_functions.sh

for platform in 'flatpak'; do
	eval "${platform}_tools_cli=\$(rg '\"interface\":\"cli\"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '${platform}\":null' | sed 's/.*${platform}\":\\([^,]*\\).*/\\1/' | uniq | head -n 1)"
	eval "${platform}_tools_gui=\$(rg '\"interface\":\"gui\"' ~/downloads_dispvm/SetupAndUpkeep/Tools.json | rg -v '${platform}\":null' | sed 's/.*${platform}\":\\([^,]*\\).*/\\1/' | uniq | head -n 1)"
done

flatpak --user remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak --user update
flatpak --user install ${flatpak_tools_cli[@] ${flatpak_tools_gui[@]}}

full_update

#/usr/bin/env pwsh

# Powershell ExecutionPolicy
# wie?

# Windows Settings -> Display Resolution
# Einstellungen -> Bildschrim -> Bildschirmauflösung
# Einstellungen -> Bildschrim -> Skalierung
# Einstellungen -> Bluetooth und Geräte -> Touchpad -> Scrollen und Zoomen
#   -> Scrollrichtung: "Zum Abwärtsscrollen nach unten bewegen"

# Pairing Bluetooth Devices securely
#   How?

# Headset Einstellungen

# bereits installierte programme durch bspw. ansible, salt, intune, ...
# -

    eza
    less
    ntop
    
    'Chocolatey.Chocolatey'
    'Microsoft.WindowsTerminal'
    'Microsoft.PowerShell'

    'Microsoft.PowerToys'
    'Brave.Brave'
    'Neovim.Neovim'
    'Git.Git'
    'GnuPG.GnuPG'
    'TheDocumentFoundation.LibreOffice'
    'Xournal++.Xournal++'
    'JohnMacFarlane.Pandoc'
    'Insecure.Nmap'
    'powertoys'
    'WiresharkFoundation.Wireshark'
    'WireGuard.WireGuard'
    #'tailscale.tailscale'
    #'Syncthing.Syncthing'
    'frippery.busybox-w32'
    'junegunn.fzf'
    'BurntSushi.ripgrep.GNU'
    'sharkdp.fd'
    'sharkdp.bat'
    'Docker.DockerCLI'
    #'Docker.DockerDesktop'
    'python3'
    'OpenJS.NodeJS'
    'powertoys'
    'LLVM.LLVM'

$programme = @(
    'powertoys'
    'microsoft-windows-terminal'
    'brave'
    'neovim'
    'git'
    'gpg4win' #gnupg is was anderes?
    #'borgbackup'
    'syncthing'
    'libreoffice'
    'xournalplusplus'
    'pandoc'
    'nmap'
    'wireshark'
    'wireguard'
    #'tailscale'
    'busybox'
    'fzf'
    'ripgrep'
    'fd'
    'docker-cli'
    'docker-engine'
    #'docker-desktop'
    'python3'
    #'python3-virtualenv'
    'nodejs'
    'ansiblevaultcmd'
    'iperf3'
    'mingw'
)

$junk = @(
    
)

# windows updates
# hersteller updates mit hersteller software (auch peripheriegeräte)
# organisation bzw. unternehmens portal updates?

# https://chocolatey.org/install
winget install chocolatey
# - falls kein winget dann mit install script?

# if statement für wenn pwsh nicht existiert und powershell-core nicht installiert
powershell.exe

choco uninstall -y $junk
choco upgrade -y chocolatey
choco install -y powershell-core
pwsh.exe
# Einstellung windows-terminal standard
# probleme mit strg taste in windows terminal?
# Einsellung powertoys tasten + fancyzones
update-help
Install-Module -Name PSWindowsUpdate -Force
Install-Module -Name PSFzf -Scope CurrentUser -Force

choco install -y $programme
choco upgrade -y all

nvim --headless "+Lazy! sync" +TSUpdate +MasonUpdate +qa

pwsh
Install-WindowsUpdate -Force
# ?Get-WindowsUpdate

# firmware updates?

# (get configs from gitlab)
# powertoys?
# https://www.makeuseof.com/powertoys-transfer-settings/
#    wie mit powershell benutzen?

# * Windows Settings -> Download Language Packs (everything, but did not work?) for German and Farsi (not default setting though)
# * Windows Settings -> Keyboard German
# * slmgr -dlv  UND slmgr -rearm
# * Install-Module PSWindwosUpdate  Yes bei NuGet und UntrustedRepo (psgallery)
# * https://github.com/microsoft/winget-cli/issues/1781
# * Install-Module -Name Microsoft.WinGet.Client  # yes zu untrustedRepo
# * winget will nicht -.-
# * https://docs.chocolatey.org/en-us/choco/setup
# * choco install microsoft-windows-terminal    # run scripts with A
# * Windows Settings -> Windows-Terminal standard Terminal
# * Taskbar -> Windows Terminal
# * choco install neovim # A to all scripts
# * choco install powertoys # A
# * Powertoys -> esc<->capslock + zones
# * choco install brave # set as default browser  + no data
#       was heißt no data?  aufjedenfall keine pw speichern und bitwarden drauf
# * WHAT is storage sense (got auto activated)
# * QUBES app detection funktioniert nicht bei windows termial (aber brave)

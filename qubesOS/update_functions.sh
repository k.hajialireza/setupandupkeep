
full_update() {
	package_manager_update
	flatpak --user update
	# appimage?
	pwsh -Command update-help
	nvim --headless "+Lazy! sync" +TSUpdate +MasonUpdate +qa
	#sudo -E containerlab version upgrade
	#arduino-cli update
	#arduino-cli upgrade
	# arduino cores and stuff?
	# per python venv
	#python3 -m pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 python3 -m pip install -U 
}

package_manager_update() {
	if [ $(grep ^ID /etc/os-release) == 'ID=fedora' ]; then
		sudo dnf upgrade
	elif [ $(grep ^ID /etc/os-release) == 'ID=debian' ]; then
		sudo apt update
		sudo apt upgrade
	fi
}
